#include <assert.h>
#include <stdio.h>

#include "mem.h"

static void test_allocation() {
    void* test_heap = heap_init(0);
    debug_heap(stdout, test_heap);

    void *allocated_memory = _malloc(0);
    assert(allocated_memory);

    debug_heap(stdout, test_heap);
    heap_term();
}

void test_free_block() {
    void* first_block = _malloc(30);
    void* second_block = _malloc(60);

    _free(first_block);
    _free(second_block);
}


void test_split_for_new_region() {
    void* test_heap = heap_init(2000);
    debug_heap(stdout, test_heap);

    void *test_block = _malloc(2001);
    debug_heap(stdout, test_heap);

    assert(test_block);
    _free(test_block);
}

int main() {
    test_allocation();
    test_free_block();
    test_split_for_new_region();
    return 0;
}
