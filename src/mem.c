#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool block_is_big_enough( size_t query, struct block_header* block ) {
    return block->capacity.bytes >= query;
}

static size_t pages_count ( size_t mem ) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages ( size_t mem ) {
    return getpagesize() * pages_count( mem );
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) {
    return size_max( round_pages( query ), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );

static void* block_after( struct block_header const* block )              {
    return  (void*) (block->contents + block->capacity.bytes);
}
bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
    // Желаемый адрес начала, количество байт, ..., ..., дескриптор файла, сдвиг
}

/*  аллоцировать регион памяти и инициализировать его блоком */
struct region alloc_region  ( void const * addr, size_t query ) {

    block_capacity current_block_capacity = {.bytes = query};
    block_size current_block_size = size_from_capacity(current_block_capacity);

    current_block_size.bytes = region_actual_size(current_block_size.bytes);
    void *current_block_address = map_pages(addr, current_block_size.bytes, MAP_FIXED_NOREPLACE);

    if (current_block_address == MAP_FAILED) {
        current_block_address = map_pages(addr, current_block_size.bytes, 0);
        if (current_block_address == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    block_init(current_block_address, current_block_size, NULL);

    bool current_block_extends = (current_block_address == addr);
    struct region mapped_region = {};

    mapped_region.addr = current_block_address;
    mapped_region.size = current_block_size.bytes;
    mapped_region.extends = current_block_extends;

    return mapped_region;
}

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if (region_is_invalid(&region)) {
        return NULL;
    }

    return region.addr;
}
void term(void* hh, size_t hs) {
    munmap(hh, hs);
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    void* heap_head = HEAP_START;
    size_t heap_size = 0;

    for (struct block_header* block = heap_head, *after_block; block; block = after_block) {
        block_capacity block_capacity = (*block).capacity;
        block_size block_size = size_from_capacity(block_capacity);

        heap_size += block_size.bytes;
        struct block_header* next_next_block = (*block).next;
        after_block = next_next_block;

        if (!blocks_continuous(block, after_block)) {
            term(heap_head, heap_size);

            heap_size = 0;
            heap_head = after_block;
        }
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой)--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    /*  разделить блок на query и остаток */
    if (!block_splittable(block, query)) {return false;}
    struct block_header* old_b_next = (*block).next;
    block_capacity old_b_capacity = (*block).capacity;
    uint8_t* old_b_contents = (*block).contents;

    block_capacity current_b_capacity;
    current_b_capacity.bytes = query;
    (*block).capacity = current_b_capacity;

    void* new_b_addr = old_b_contents + current_b_capacity.bytes;

    block_size new_b_size;
    new_b_size.bytes = old_b_capacity.bytes - current_b_capacity.bytes;

    block_init(new_b_addr, new_b_size, old_b_next);

    struct block_header* current_b_next = (struct block_header *) (new_b_addr);
    (*block).next = current_b_next;

    return true;

}

///*  --- Слияние соседних свободных блоков --- */

static bool mergeable(
        struct block_header const* restrict fst,
        struct block_header const* restrict snd
                ) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (!block) {return false;}

    struct block_header* next_block = (*block).next;
    if (!next_block || !mergeable(block, next_block)) {return false;}

    block_capacity next_block_capacity = (*next_block).capacity;
    struct block_header *next_block_next = (*next_block).next;

    block_size next_block_size = size_from_capacity(next_block_capacity);
    (*block).capacity.bytes += next_block_size.bytes;
    (*block).next = next_block_next;

    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;

  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {

    struct block_search_result good_res;
    good_res.type = BSR_FOUND_GOOD_BLOCK;

    struct block_search_result not_found_res;
    not_found_res.type = BSR_REACHED_END_NOT_FOUND;

    struct block_header* previous_block = block;
    while (block) {
        while (try_merge_with_next(block)) {};

        bool block_is_free = (*block).is_free;
        struct block_header* next_block = (*block).next;

        if (block_is_big_enough(sz, block) && block_is_free) {
            good_res.block = block;
            return good_res;
        }
        previous_block = block;
        block = next_block;
    }

    not_found_res.block = previous_block;
    return not_found_res;
}

/*
 * Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 * Можно переиспользовать как только кучу расширили.
*/
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    if (block != NULL) {
        struct block_search_result good_or_last = find_good_or_last(block, query);
        if (good_or_last.type == BSR_FOUND_GOOD_BLOCK) {
            struct block_header* find_block = good_or_last.block;
            split_if_too_big(find_block, query);
            (*find_block).is_free = false;
        }
        return good_or_last;
    }
    else {
        struct block_search_result corrupted_res;
        corrupted_res.type = BSR_CORRUPTED;
        corrupted_res.block = NULL;

        return corrupted_res;

    }
}


static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct block_header* addr = NULL;
    if (last == NULL) {return addr;}

    void* start_block = block_after(last);
    struct region region = alloc_region(start_block, query);

    if (!region_is_invalid(&region)) {
        addr = region.addr;
        (*last).next = region.addr;
        if (try_merge_with_next(last)) {
            addr = last;
        }
    }
    return addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    struct block_header* addr = NULL;
    if (heap_start == NULL){return addr;}

    query = size_max(query, BLOCK_MIN_CAPACITY);

    struct block_search_result memalloc_result = try_memalloc_existing(query, heap_start);
    struct block_header* memalloc_result_block = memalloc_result.block;

    if(memalloc_result.type == BSR_CORRUPTED) {return NULL;}

    if (memalloc_result.type == BSR_FOUND_GOOD_BLOCK){
        return memalloc_result_block;
    }
    struct block_header* new_block = grow_heap(memalloc_result_block, query);
    struct block_search_result final_result = try_memalloc_existing(query, new_block);
    struct block_header* final_block = final_result.block;

    return final_block;

}


void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
    while(try_merge_with_next(header));
}
